*** Settings ***
Resource        ../resources/Resource.robot
Resource        ../resources/pageobjects/TransportadoraPage.robot
Resource        ../resources/pageobjects/HomePage.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Cases ***
Caso de Teste 01: Validar se usuário consegui inputar Código 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário inputar Código
    TransportadoraPage.Então o Código inputado ficará evidente no textfield

Caso de Teste 02: Validar se usuário consegui inputar Nome 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário inputar Nome
    TransportadoraPage.Então o Nome inputado ficará evidente no textfield

Caso de Teste 03: Validar se usuário consegui inputar Nome Reduzido
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário inputar Nome Reduzido
    TransportadoraPage.Então o Nome Reduzido inputado ficará evidente no textfield

Caso de Teste 04: Validar se usuário consegui incluir uma Transportadora com sucesso 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário preencher todos os campos obrigatórios e clicar no botão Incluir
    TransportadoraPage.Então usuário irá incluir uma Transportadora com sucesso 

Caso de Teste 05: Validar se usuário consegui visualizar mensagem de erro ao tentar incluir uma transportadora com dados inconsistente
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário preencher os campos com dados inconsistente e clicar no botão Incluir
    TransportadoraPage.Então usuário irá visualizar uma mensagem de alerta relatando erro na inclusão

Caso de Teste 06: Validar se usuário consegui visualizar modal deletar
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário selecionar uma Transportadora e clicar no botão Excluir
    TransportadoraPage.Então usuário irá visualizar modal Deletar

Caso de Teste 07: Validar se usuário consegui fechar modal deletar 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário selecionar uma Transportadora, clicar no botão Excluir em seguida no botão Cancelar
    TransportadoraPage.Então usuário irá fechar modal Deletar

Caso de Teste 08: Validar se usuário consegui deletar com sucesso uma Transportado
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário selecionar uma Transportadora, clicar no botão Excluir em seguida no botão OK
    TransportadoraPage.Então usuário irá Excluir uma Transportadora com sucesso

Caso de Teste 09: Validar se usuário consegui editar uma Transportadora com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário selecionar uma Transportadora, alterar dados em seguida clicar no botão Editar
    TransportadoraPage.Então usuário irá Editar uma Transportadora com sucesso

Caso de Teste 10: Validar se apresenta o alerta de erro ao tentar editar uma Transportadora com dados inconsistente 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário selecionar uma Transportadora, alterar informações com dados inconsistente em seguida clicar no botão Editar
    TransportadoraPage.Então usuário irá visualizar uma mensagem de alerta

Caso de Teste 11: Validar se usuário consegui alterar o radio button Inf Manual Entregador
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário clicar no radio button Inf Manual Entregador
    TransportadoraPage.Então usuário irá alterar o radio button Inf Manual Entregador

Caso de Teste 12: Validar se usuário consegui alterar o radio button Inf Manual Entrega
    HomePage.Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    TransportadoraPage.Quando usuário clicar no radio button Inf Manual Entrega
    TransportadoraPage.Então usuário irá alterar o radio button Inf Manual Entrega