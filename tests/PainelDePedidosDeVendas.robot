*** Settings ***
Resource        ../resources/Resource.robot
Resource        ../resources/pageobjects/PainelDePedidosDeVendasPage.robot
Resource        ../resources/pageobjects/HomePage.robot
Test Setup      Abrir navegador headless
Test Teardown   Fechar navegador

*** Test Cases ***
Caso de Teste 01: Validar se usuário consegui visualizar o card Aguardando Confirmação
    HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
    PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
    PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Aguardando Confirmação

# Caso de Teste 02: Validar se usuário consegui visualizar o card Em Rota de Entrega
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Em Rota de Entrega
    
# Caso de Teste 03: Validar se usuário consegui visualizar o card Total de Pedidos
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Total de Pedidos
    
# Caso de Teste 04: Validar se usuário consegui visualizar o card Aguardando Pagamento
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Aguardando Pagamento
    
# Caso de Teste 05: Validar se usuário consegui visualizar o card Aguardando Separação
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Aguardando Separação
    
# Caso de Teste 06: Validar se usuário consegui visualizar o card Aguardando Expedição 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Aguardando Expedição 
    
# Caso de Teste 07: Validar se usuário consegui visualizar o card Entregues 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Entregues
    
# Caso de Teste 08: Validar se usuário consegui visualizar o card Cancelados
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Cancelados
    
# Caso de Teste 09: Validar se usuário consegui visualizar o card Problemas Entrega 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Problemas Entrega 
    
# Caso de Teste 10: Validar se usuário consegui visualizar o card Problemas Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Problemas Transportadora 
    
# Caso de Teste 11: Validar se usuário consegui visualizar o card Receita
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Receita
    
# Caso de Teste 12: Validar se usuário consegui visualizar o card Ticket Médio
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Ticket Médio
    
# Caso de Teste 13: Validar se usuário consegui visualizar o card Risco de Atraso
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Risco de Atraso
    
# Caso de Teste 14: Validar se usuário consegui visualizar o card Entregues com atraso
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar o card Entregues com atraso
    
# Caso de Teste 15: Validar se usuário consegui visualizar modal Filtros após clicar no ícone Filtro de Pedidos
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos 
#     PainelDePedidosDeVendasPage.Então usuário irá conseguir visualizar modal Filtros
    
# Caso de Teste 16: Validar se usuário consegui fechar modal Filtros após clicar no ícone de fechar (X) 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos em seguida no ícone de fechar (X)
#     PainelDePedidosDeVendasPage.Então usuário irá conseguir fechar a modal Filtros
    
# Caso de Teste 17: Validar se usuário consegui selecionar uma opção no comboBox Seller
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Seller
#     PainelDePedidosDeVendasPage.Então a opção selecionada no comboBox Seller ficará evidente 
    
# Caso de Teste 18: Validar se usuário consegui selecionar uma opção no comboBox Web Store
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Web Store
#     PainelDePedidosDeVendasPage.Então a opção selecionada no comboBox Web Store ficará evidente 
    
# Caso de Teste 19: Validar se usuário consegui selecionar uma opção no comboBox Lojas
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Lojas
#     PainelDePedidosDeVendasPage.Então a opção selecionada no comboBox Lojas ficará evidente
    
# Caso de Teste 20: Validar se usuário consegui inputar Data Início
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos e inputar Data Início
#     PainelDePedidosDeVendasPage.Então a Data Início inputada ficará evidente no textfield
    
# Caso de Teste 21: Validar se usuário consegui inputar Data Fim
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos e inputar Data Fim
#     PainelDePedidosDeVendasPage.Então a Data Fim inputada ficará evidente no textfield
    
# Caso de Teste 22: Validar se usuário consegui selecionar uma opção no comboBox Status
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Status
#     PainelDePedidosDeVendasPage.Então a opção selecionada no comboBox Status ficará evidente
    
# Caso de Teste 23: Validar se usuário consegui selecionar uma opção no comboBox Pendências 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Pendências
#     PainelDePedidosDeVendasPage.Então a opção selecionada no comboBox Pendências ficará evidente
    
# Caso de Teste 24: Validar se usuário consegui fechar modal Filtros após clicar no botão Cancelar 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos em seguida no botão Cancelar 
#     PainelDePedidosDeVendasPage.Então usuário conseguirá fechar modal Filtros
    
# Caso de Teste 25: Validar se usuário consegui limpar dados inputados após clicar no botão Limpar
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone Filtro de Pedidos, preencher os campos e em seguida no botão Limpar
#     PainelDePedidosDeVendasPage.Então usuário conseguirá limpar dados digitado nos campos
    
# Caso de Teste 26: Validar se usuário consegui atualizar Filtro de Pedidos após clicar no botão Atualizar 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Atualizar
#     PainelDePedidosDeVendasPage.Então usuário irá consegui atualizar filtro de pedidos
    
# Caso de Teste 27: Validar se usuário consegui visualizar modal Filtros após clicar no botão Buscar 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Buscar
#     PainelDePedidosDeVendasPage.Então usuário irá conseguir visualizar modal Filtros
    
# Caso de Teste 28: Validar se após usuário clicar no botão Buscar ele consegui digitar no textfield Número do Pedido 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Buscar e digitar no textfield Número do Pedido 
#     PainelDePedidosDeVendasPage.Então o Número do Pedido inputada ficará evidente no textfield
    
# Caso de Teste 29: Validar se após usuário clicar no botão Buscar ele consegui digitar no textfield Nome de Cliente
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Buscar e digitar no textfield Nome de Cliente
#     PainelDePedidosDeVendasPage.Então o Nome de Cliente inputada ficará evidente no textfield
    
# Caso de Teste 30: Validar se usuário consegui fechar a modal Filtros após usuário clicar no botão Buscar em seguida no ícone de fechar (X) 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Buscar em seguida no ícone de fechar (X)
#     PainelDePedidosDeVendasPage.Então usuário irá conseguir fechar a modal Filtros
    
# Caso de Teste 31: Validar se usuário consegui realizar download do arquivo XLS dos pedidos
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Exportar em XLS
#     PainelDePedidosDeVendasPage.Então usuário irá conseguir realizar download do arquivo 
    
# Caso de Teste 32: Validar se usuário consegui visualizar tabela de pedidos
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário estiver na aba Painel de Pedidos de Vendas
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar a tabela de pedidos

# Caso de Teste 33: Validar se usuário consegui concluir um filtro de pedido com êxito pela modal filtro 
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário preencher todos os campos obrigatórios na modal Filtro de Pedidos e clicar em Confirmar 
#     PainelDePedidosDeVendasPage.Então usuário irá conseguir visualizar na tabela de pedidos os dados do pedido informado na modal Filtro

# Caso de Teste 34: Validar se usuário consegui concluir um filtro de pedido com êxito pelo Número do Pedido
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário preencher Número do Pedido e clicar em Buscar
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar na tabela de pedidos os dados da Buscar pelo Número do Pedido

# Caso de Teste 35: Validar se usuário consegui concluir um filtro de pedido com êxito pelo Nome de Cliente
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário preencher Nome de Cliente e clicar em Buscar
#     PainelDePedidosDeVendasPage.Então usuário irá consegui visualizar na tabela de pedidos os dados da Buscar pelo Nome de Cliente

# Caso de Teste 36: Validar se ao clicar em algum pedido da tabela usuário consegui ser direcionado para a página de Detalhamento do Pedido
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar em algum pedido da tabela
#     PainelDePedidosDeVendasPage.Então usuário será direcionado para a página de Detalhamento do Pedido

# Caso de Teste 37: Validar se apresenta o alerta de erro ao tentar filtrar um pedido inconsistente 
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no ícone filtro e preencher dados inconsistente
#     PainelDePedidosDeVendasPage.Então usuário irá visualizar uma mensagem de alerta 

# Caso de Teste 38: Validar se apresenta o alerta de erro ao tentar filtrar um pedido inconsistente por Número de Pedido
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Buscar e efetuar uma buscar por Número de Pedido inconsistente
#     PainelDePedidosDeVendasPage.Então usuário irá visualizar uma mensagem de alerta 

# Caso de Teste 39: Validar se apresenta o alerta de erro ao tentar filtrar um pedido inconsistente por Nome de Cliente
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário clicar no botão Buscar e efetuar uma buscar por Nome de Cliente inconsistente
#     PainelDePedidosDeVendasPage.Então usuário irá visualizar uma mensagem de alerta 

# Caso de Teste 40: Validar se usuário consegui inputar Nome do Entregador em Dados da Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a modal Filtros e digitar no textfield Nome do Entregador
#     PainelDePedidosDeVendasPage.Então o Nome do Entregador inputada ficará evidente no textfield 

# Caso de Teste 41: Validar se usuário consegui inputar Documento em Dados da Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a modal Filtros e digitar no textfield Documento
#     PainelDePedidosDeVendasPage.Então o Documento inputada ficará evidente no textfield 

# Caso de Teste 42: Validar se usuário consegui inputar Placa de Veículo em Dados da Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a modal Filtros e digitar no textfield Placa de Veículo
#     PainelDePedidosDeVendasPage.Então a Placa de Veículo inputada ficará evidente no textfield 

# Caso de Teste 43: Validar se usuário consegui inputar Contato em Dados da Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido e digitar no textfield Contato
#     PainelDePedidosDeVendasPage.Então o Contato inputada ficará evidente no textfield 

# Caso de Teste 44: Validar se o botão Mapa de Rastreio de Entrega apresenta habilitado para pedidos com status de Em Rota de Entrega
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega
#     PainelDePedidosDeVendasPage.Então o botão Mapa de Rastreio de Entrega deve apresentar habilitado para receber click 

# Caso de Teste 45: Validar se o botão Comprovante de Entrega apresenta habilitado para pedidos com status de Entregue
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Entregue
#     PainelDePedidosDeVendasPage.Então o botão Comprovante de Entrega deve apresentar habilitado para receber click 

# Caso de Teste 46: Validar se o botão Entrega Efetiva apresenta habilitado para pedidos com status de Em Rota de Entrega
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega
#     PainelDePedidosDeVendasPage.Então o botão Entrega Efetiva deve apresentar habilitado para receber click

# Caso de Teste 47: Validar se usuário consegui visualizar modal Entrega Efetiva após clicar no botão Entrega Efetiva 
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega e clicar no botão Entrega Efetiva
#     PainelDePedidosDeVendasPage.Então usuário irá conseguir visualizar modal Entrega Efetiva

# Caso de Teste 48: Validar se usuário consegui fechar a modal Entrega Efetiva após usuário clicar no botão Entrega Efetiva em seguida no ícone de fechar (X) 
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega, clicar no botão Entrega Efetiva em seguida no ícone de fechar (X) 
#     PainelDePedidosDeVendasPage.Então irá fechar a modal Entrega Efetiva

# Caso de Teste 49: Validar se usuário consegui inputar Data na modal Entrega Efetiva
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega, clicar no botão Entrega Efetiva e inputar Data
#     PainelDePedidosDeVendasPage.Então a Data inputada ficará evidente no textfield 

# Caso de Teste 50: Validar se usuário consegui inputar Hora na modal Entrega Efetiva
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega, clicar no botão Entrega Efetiva e inputar Hora
#     PainelDePedidosDeVendasPage.Então a Hora inputada ficará evidente no textfield

# Caso de Teste 51: Validar se o botão Solicitar Coleta apresenta habilitado para pedidos com status de Aguardando Separação
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Aguardando Separação
#     PainelDePedidosDeVendasPage.Então o botão Solicitar Coleta deve apresentar habilitado para receber click 

# Caso de Teste 52: Validar se o botão Confirmar Expedição apresenta habilitado para pedidos com status de Aguardando Expedição
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Aguardando Expedição
#     PainelDePedidosDeVendasPage.Então o botão Confirmar Expedição deve apresentar habilitado para receber click 

# Caso de Teste 53: Validar se o botão Motivo de Cancelamento apresenta habilitado para pedidos com status de Cancelado
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Cancelado
#     PainelDePedidosDeVendasPage.Então o botão Motivo de Cancelamento deve apresentar habilitado para receber click 

# Caso de Teste 54: Validar se o botão Confirmar Pedido apresenta habilitado para pedidos com status de Aguardando Confirmação
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Aguardando Confirmação
#     PainelDePedidosDeVendasPage.Então o botão Confirmar Pedido deve apresentar habilitado para receber click 

# Caso de Teste 55: Validar se o botão Rejeitar Pedido apresenta habilitado para pedidos com status de Aguardando Confirmação
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido com status Aguardando Confirmação
#     PainelDePedidosDeVendasPage.Então o botão Rejeitar Pedido deve apresentar habilitado para receber click 

# Caso de Teste 56: Validar se o botão Salvar apresenta habilitado em Detalhamento do Pedido
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
#     PainelDePedidosDeVendasPage.Quando usuário acessar a página Detalhamento do Pedido
#     PainelDePedidosDeVendasPage.Então o botão Salvar deve apresentar habilitado para receber click 
    
 