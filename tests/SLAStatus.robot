*** Settings ***
Resource        ../resources/Resource.robot
Resource        ../resources/pageobjects/SLAStatusPage.robot
Resource        ../resources/pageobjects/HomePage.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Cases ***
Caso de Teste 01: Validar se usuário consegui selecionar uma opção no comboBox Tipo Entrega 
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário selecionar uma opção no comboBox Seller
    SLAStatusPage.Então a opção selecionada no comboBox Seller ficará evidente 

Caso de Teste 02: Validar se usuário consegui selecionar uma opção no comboBox Status
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário selecionar uma opção no comboBox Status
    SLAStatusPage.Então a opção selecionada no comboBox Status ficará evidente 

Caso de Teste 03: Validar se usuário consegui inputar SLA (min)
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário inputar SLA (min)
    SLAStatusPage.Então o SLA (min) inputado ficará evidente no textfield

Caso de Teste 04: Validar se usuário consegui incluir SLA Status com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário preencher todos os campos obrigatórios e clicar no botão Incluir
    SLAStatusPage.Então usuário irá incluir um SLA Status com sucesso

Caso de Teste 05: Validar se usuário consegui visualizar mensagem de Erro ao tentar incluir um SLA Status com dados inconsistente
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário preencher os campos com dados inconsistente e clicar no botão Incluir
    SLAStatusPage.Então usuário irá visualizar uma mensagem de alerta

Caso de Teste 06: Validar se usuário consegui visualizar modal deletar 
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário selecionar um SLA Status e clicar no botão Excluir
    SLAStatusPage.Então usuário irá visualizar modal Deletar

Caso de Teste 07: Validar se usuário consegui fechar modal Deletar, após selecionar um SLA Status, clicar no botão Excluir em seguida no botão Cancelar 
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário selecionar um SLA Status, clicar no botão Excluir em seguida no botão Cancelar
    SLAStatusPage.Então usuário irá fechar modal Deletar

Caso de Teste 08: Validar se usuário consegui deletar um SLA Status com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário selecionar um SLA Status, clicar no botão Excluir em seguida no botão OK
    SLAStatusPage.Então usuário irá Excluir um SLA Status com sucesso

Caso de Teste 09: Validar se usuário consegui editar SLA Status com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário selecionar um SLA Status, alterar dados em seguida clicar no botão Editar
    SLAStatusPage.Então usuário irá Editar um SLA Status com sucesso

Caso de Teste 10: Validar se apresenta o alerta de erro ao tentar editar um SLA Status com dados inconsistente 
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    SLAStatusPage.Quando usuário selecionar um SLA Status, alterar informações com dados inconsistente em seguida clicar no botão Editar
    SLAStatusPage.Então usuário irá visualizar uma mensagem de alerta