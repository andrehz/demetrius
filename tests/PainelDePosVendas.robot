*** Settings ***
Resource        ../resources/Resource.robot
Resource        ../resources/pageobjects/PainelDePosVendasPage.robot
Resource        ../resources/pageobjects/HomePage.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Cases ***
Caso de Teste 01: Validar se usuário consegui selecionar uma opção no comboBox Seller
    HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
    PainelDePosVendasPage.Quando usuário selecionar uma opção no comboBox Seller
    PainelDePosVendasPage.Então a opção selecionada no comboBox Seller ficará evidente 

# Caso de Teste 02: Validar se usuário consegui selecionar uma opção no comboBox Webstore
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário selecionar uma opção no comboBox Web Store
#     PainelDePosVendasPage.Então a opção selecionada no comboBox Web Store ficará evidente 

# Caso de Teste 03: Validar se usuário consegui selecionar uma opção no comboBox Loja
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário selecionar uma opção no comboBox Lojas
#     PainelDePosVendasPage.Então a opção selecionada no comboBox Lojas ficará evidente

# Caso de Teste 04: Validar se usuário consegui inputar Data Início
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário inputar Data Início
#     PainelDePosVendasPage.Então a Data Início inputada ficará evidente no textfield

# Caso de Teste 05: Validar se usuário consegui inputar Data Fim
#     [Setup]  Abrir navegador chrome
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário inputar Data Fim
#     PainelDePosVendasPage.Então a Data Fim inputada ficará evidente no textfield

# Caso de Teste 06: Validar se usuário consegui selecionar uma opção no comboBox Status
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário selecionar uma opção no comboBox Status
#     PainelDePosVendasPage.Então a opção selecionada no comboBox Status ficará evidente

# Caso de Teste 07: Validar se usuário consegui atualizar Filtro de Pedidos após clicar no botão Atualizar 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Atualizar
#     PainelDePosVendasPage.Então usuário irá consegui atualizar filtro de pedidos

# Caso de Teste 08: Validar se usuário consegui visualizar modal Filtros após clicar no botão Buscar 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar
#     PainelDePosVendasPage.Então usuário irá conseguir visualizar modal Filtros

# Caso de Teste 09: Validar se após usuário clicar no botão Buscar ele consegui digitar no textfield Número da Solicitação 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar e digitar no textfield Número da Solicitação 
#     PainelDePosVendasPage.Então o Número da Solicitação inputada ficará evidente no textfield

# Caso de Teste 10: Validar se após usuário clicar no botão Buscar ele consegui digitar no textfield Número do Pedido 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar e digitar no textfield Número do Pedido 
#     PainelDePosVendasPage.Então o Número do Pedido inputada ficará evidente no textfield

# Caso de Teste 11: Validar se após usuário clicar no botão Buscar ele consegui digitar no textfield Nome de Cliente
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar e digitar no textfield Nome de Cliente
#     PainelDePosVendasPage.Então o Nome de Cliente inputada ficará evidente no textfield

# Caso de Teste 12: Validar se usuário consegui limpar dados inputados após clicar no botão Limpar
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar, preencher os campos e em seguida clicar no botão Limpar
#     PainelDePosVendasPage.Então usuário conseguirá limpar dados digitado nos campos

# Caso de Teste 13: Validar se usuário consegui fechar a modal Filtros após usuário clicar no botão Buscar em seguida no ícone de fechar (X) 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar em seguida no ícone de fechar (X)
#     PainelDePosVendasPage.Então usuário irá conseguir fechar a modal Filtros

# Caso de Teste 14: Validar se apresenta o alerta de erro ao tentar filtrar um pedido inconsistente por Número da Solicitação
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar e efetuar uma buscar por Número da Solicitação inconsistente
#     PainelDePosVendasPage.Então usuário irá visualizar uma mensagem de alerta 

# Caso de Teste 15: Validar se apresenta o alerta de erro ao tentar filtrar um pedido inconsistente por Número de Pedido
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar e efetuar uma buscar por Número de Pedido inconsistente
#     PainelDePosVendasPage.Então usuário irá visualizar uma mensagem de alerta 

# Caso de Teste 16: Validar se apresenta o alerta de erro ao tentar filtrar um pedido inconsistente por Nome de Cliente
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Buscar e efetuar uma buscar por Nome de Cliente inconsistente
#     PainelDePosVendasPage.Então usuário irá visualizar uma mensagem de alerta 

# Caso de Teste 17: Validar se ao clicar no botão Abrir Nova Solicitação usuário é direcionado para página Solicitação Troca/Cancelamento
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário clicar no botão Abrir Nova Solicitação
#     PainelDePosVendasPage.Então usuário será direcionado para a página de Solicitação Troca/Cancelamento

# Caso de Teste 18: Validar se usuário consegui selecionar uma opção no comboBox Tipo Solicitação em Dados da Solicitação
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e selecionar uma opção no comboBox Tipo Solicitação
#     PainelDePosVendasPage.Então a opção selecionada no comboBox Tipo Solicitação ficará evidente 

# Caso de Teste 19: Validar se usuário consegui selecionar uma opção no comboBox Motivo em Dados da Solicitação
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e selecionar uma opção no comboBox Motivo
#     PainelDePosVendasPage.Então a opção selecionada no comboBox Motivo ficará evidente 

# Caso de Teste 20: Validar se usuário consegui inputar dados no textarea Observações adicionais em Dados da Solicitação
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados no textarea Observações adicionais
#     PainelDePosVendasPage.Então as Observações adicionais inputada ficará evidente no textarea 

# Caso de Teste 21: Validar se usuário consegui inputar dados no textfield Pedido de Venda em Dados do Pedido
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados no textfield Pedido de Venda
#     PainelDePosVendasPage.Então o Pedido de Venda inputada ficará evidente no textfield

# Caso de Teste 22: Validar se usuário consegui filtrar um Pedido de Venda com sucesso
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados no textfield Pedido de Venda em seguida clicar no ícone de pesquisa
#     PainelDePosVendasPage.Então usuário irá consegui filtrar um Pedido de Venda com sucesso

# Caso de Teste 23: Validar se usuário consegui visualizar a mensagem de alerta caso ele efetua um filtro de um Pedido de Venda inconsistente
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados inconsistente no textfield Pedido de Venda em seguida clicar no ícone de pesquisa
#     PainelDePosVendasPage.Então usuário irá visualizar uma mensagem de alerta

# Caso de Teste 24: Validar se usuário consegui inputar Nome do Entregador em Dados da Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e digitar no textfield Nome do Entregador
#     PainelDePosVendasPage.Então o Nome do Entregador inputada ficará evidente no textfield 

# Caso de Teste 25: Validar se usuário consegui inputar Documento em Dados da Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e digitar no textfield Documento
#     PainelDePosVendasPage.Então o Documento inputada ficará evidente no textfield 

# Caso de Teste 26: Validar se usuário consegui inputar Placa de Veículo em Dados da Transportadora 
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento e digitar no textfield Placa de Veículo
#     PainelDePosVendasPage.Então a Placa de Veículo inputada ficará evidente no textfield 

# Caso de Teste 27: Validar se o botão Solicitar Coleta apresenta habilitado para pedidos com status de Aguardando Solicitação Coleta
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Aguardando Solicitação Coleta
#     PainelDePosVendasPage.Então o botão Solicitar Coleta deve apresentar habilitado para receber click 

# Caso de Teste 28: Validar se o botão Confirmar Retorno da Coleta apresenta habilitado para pedidos com status Em Coleta
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Coleta
#     PainelDePosVendasPage.Então o botão Confirmar Retorno da Coleta deve apresentar habilitado para receber click 

# Caso de Teste 29: Validar se o botão Encerrar Solicitação apresenta habilitado para pedidos com status Coleta Realizada
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Coleta Realizada
#     PainelDePosVendasPage.Então o botão Encerrar Solicitação deve apresentar habilitado para receber click 

# Caso de Teste 30: Validar se o botão Cancelar Solicitação apresenta habilitado para pedidos com status Em Aprovação Seller
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Aprovação Seller
#     PainelDePosVendasPage.Então o botão Cancelar Solicitação deve apresentar habilitado para receber click 

# Caso de Teste 31: Validar se o botão Imprimir Ordem de Coleta apresenta habilitado para pedidos com status Em Aprovação Seller
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Aprovação Seller
#     PainelDePosVendasPage.Então o botão Imprimir Ordem de Coleta deve apresentar habilitado para receber click 

# Caso de Teste 32: Validar se o botão Salvar Solicitação apresenta habilitado para pedidos com status Em Aprovação Seller
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Aprovação Seller
#     PainelDePosVendasPage.Então o botão Salvar Solicitação deve apresentar habilitado para receber click 

# Caso de Teste 33: Validar se o botão Aprovar apresenta habilitado para pedidos com status Em Aprovação Seller
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Aprovação Seller
#     PainelDePosVendasPage.Então o botão Aprovar deve apresentar habilitado para receber click 

# Caso de Teste 34: Validar se o botão Reprovar apresenta habilitado para pedidos com status Em Aprovação Seller
#     HomePage.Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
#     PainelDePosVendasPage.Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Aprovação Seller
#     PainelDePosVendasPage.Então o botão Reprovar deve apresentar habilitado para receber click 