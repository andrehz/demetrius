*** Settings ***
Library     SeleniumLibrary
Library     String
Resource    ../pageobjects/HomePage.robot

*** Variables ***
${LOGIN_URL}                         https://parceirofast-develop.fastshop.com.br/
${LOGIN_URL_QA}                      https://parceirofast-qa.fastshop.com.br/
${LOGIN_TITLE}                       Parceiro Fast
${LOGIN_TITLE_HOME}                  ParceiroFast | Home 
${LOGIN_EMAIL}                       id:userName
${LOGIN_SENHA}                       id:Password
${LOGIN_BTN_ENTRAR}                  id:login
