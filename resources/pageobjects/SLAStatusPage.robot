*** Settings ***
Library     DateTime
Library     OperatingSystem
Library     SeleniumLibrary
Library     String
Library     FakerLibrary    locale=pt_BR
Resource    ../pageobjects/HomePage.robot

*** Variables ***
${SLA_STATUS_URL}                        https://parceirofast-develop.fastshop.com.br/home/painel
${SLA_STATUS_TITLE}                      Parceiro Fast | SLA - Tipo Status
${SLA_STATUS_COMBO_TIPO_DA_ENTREGA}      id:comboTipoEntrega
${SLA_STATUS_COMBO_STATUS}               id:comboStatus
${SLA_STATUS_TEXTFIELD_SLA_MIN}          id:slaEtapaMin
${SLA_STATUS_BTN_INCLUIR}                id:btnIncluirEtapa
${SLA_STATUS_BTN_EXCLUIR}                id:btnExcluirEtapa
${SLA_STATUS_BTN_EDITAR}                 id:btnEditarEtapa
${SLA_STATUS_TABLE}                      id:gridSlaEtapa
${SLA_STATUS_BTN_CANCELAR}               id:buttonSubmitCancel
${SLA_STATUS_BTN_APAGAR}                 id:buttonSubmitApagar

*** Keywords ***
#Caso de Teste 01
Quando usuário selecionar uma opção no comboBox Seller
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_COMBO_TIPO_DA_ENTREGA}  30sec
    Click Element                  ${SLA_STATUS_COMBO_TIPO_DA_ENTREGA}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'Engenheiro agrônomo')] 

Então a opção selecionada no comboBox Seller ficará evidente 
    ${selected}=                   Get Text  ${SLA_STATUS_COMBO_TIPO_DA_ENTREGA}
    Should Be Equal                ${selected}  Engenheiro agrônomo  

#Caso de Teste 02
Quando usuário selecionar uma opção no comboBox Status
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_COMBO_STATUS}  30sec
    Click Element                  ${SLA_STATUS_COMBO_STATUS}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'Entregue')] 

Então a opção selecionada no comboBox Status ficará evidente 
    ${selected}=                   Get Text  ${SLA_STATUS_COMBO_STATUS}
    Should Be Equal                ${selected}  Entregue  

#Caso de Teste 03
Quando usuário inputar SLA (min)
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_TEXTFIELD_SLA_MIN}  30sec
    ${SLA_MIN}=                    generate random string  2  [NUMBERS]
    Set Test Variable              ${SLA_MIN}
    Input Text                     ${SLA_STATUS_TEXTFIELD_SLA_MIN}  ${SLA_MIN}

Então o SLA (min) inputado ficará evidente no textfield
    Textfield Should Contain       ${SLA_STATUS_TEXTFIELD_SLA_MIN}  ${SLA_MIN}

#Caso de Teste 04
Quando usuário preencher todos os campos obrigatórios e clicar no botão Incluir
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_COMBO_TIPO_DA_ENTREGA}  30sec
    Click Element                  ${SLA_STATUS_COMBO_TIPO_DA_ENTREGA}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'Médico legista')] 
    Wait Until Element Is Visible  ${SLA_STATUS_COMBO_STATUS}  30sec
    Click Element                  ${SLA_STATUS_COMBO_STATUS}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'Entregue')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_TEXTFIELD_SLA_MIN}  30sec
    ${SLA_MIN}=                    generate random string  2  [NUMBERS]
    Set Test Variable              ${SLA_MIN}
    Input Text                     ${SLA_STATUS_TEXTFIELD_SLA_MIN}  ${SLA_MIN}
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_INCLUIR}  30sec
    Click Element                  ${SLA_STATUS_BTN_INCLUIR} 

Então usuário irá incluir um SLA Status com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 05
Quando usuário preencher os campos com dados inconsistente e clicar no botão Incluir
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_INCLUIR}  30sec
    Click Element                  ${SLA_STATUS_BTN_INCLUIR}

Então usuário irá visualizar uma mensagem de alerta
    Wait Until Element Is Visible  xpath://span[contains(.,'ERRO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 06
Quando usuário selecionar um SLA Status e clicar no botão Excluir
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Engenheiro agrônomo')]  30sec
    Click Element                  xpath://td[contains(.,'Engenheiro agrônomo')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_EXCLUIR}
    Click Element                  ${SLA_STATUS_BTN_EXCLUIR}
    Sleep  5
    
Então usuário irá visualizar modal Deletar
    Page Should Contain            Apagar sla status  

#Caso de Teste 07
Quando usuário selecionar um SLA Status, clicar no botão Excluir em seguida no botão Cancelar
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Engenheiro agrônomo')]  30sec
    Click Element                  xpath://td[contains(.,'Engenheiro agrônomo')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_EXCLUIR}
    Click Element                  ${SLA_STATUS_BTN_EXCLUIR}
    Sleep  5
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_CANCELAR}
    Click Element                  ${SLA_STATUS_BTN_CANCELAR}

Então usuário irá fechar modal Deletar
    Sleep  5
    Page Should Not Contain        Apagar sla status

#Caso de Teste 08
Quando usuário selecionar um SLA Status, clicar no botão Excluir em seguida no botão OK
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Engenheiro agrônomo')]  30sec
    Click Element                  xpath://td[contains(.,'Engenheiro agrônomo')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_EXCLUIR}
    Click Element                  ${SLA_STATUS_BTN_EXCLUIR}
    Sleep  5
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_APAGAR}
    Click Element                  ${SLA_STATUS_BTN_APAGAR}

Então usuário irá Excluir um SLA Status com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 09
Quando usuário selecionar um SLA Status, alterar dados em seguida clicar no botão Editar
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'test')]  30sec
    Click Element                  xpath://td[contains(.,'test')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_EDITAR}
    Click Element                  ${SLA_STATUS_BTN_EDITAR}

Então usuário irá Editar um SLA Status com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 10
Quando usuário selecionar um SLA Status, alterar informações com dados inconsistente em seguida clicar no botão Editar
    Sleep  2
    Wait Until Element Is Visible  ${SLA_STATUS_BTN_EDITAR}
    Click Element                  ${SLA_STATUS_BTN_EDITAR}
    Sleep  5




