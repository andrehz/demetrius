*** Settings ***
Library     DateTime
Library     OperatingSystem
Library     SeleniumLibrary
Library     String
Library     FakerLibrary    locale=pt_BR
Resource    ../pageobjects/HomePage.robot

*** Variables ***
${TRANSPORTADORA_URL}                        https://parceirofast-develop.fastshop.com.br/home/painel
${TRANSPORTADORA_TITLE}                      Parceiro Fast | Transportadora
${TRANSPORTADORA_TEXTFIELD_CODIGO}           id:codigo
${TRANSPORTADORA_TEXTFIELD_NOME}             id:nome
${TRANSPORTADORA_TEXTFIELD_NOME_REDUZIDO}    id:nomeReduzido
${TRANSPORTADORA_BTN_INCLUIR}                id:btnIncluir
${TRANSPORTADORA_BTN_EXCLUIR}                id:btnExcluir
${TRANSPORTADORA_BTN_EDITAR}                 id:btnEditar
${TRANSPORTADORA_TABLE}                      id:gridParametrizacao
${TRANSPORTADORA_BTN_CANCELAR}               id:buttonSubmitCancel
${TRANSPORTADORA_BTN_APAGAR}                 id:buttonSubmitApagar

*** Keywords ***
#Caso de Teste 01
Quando usuário inputar Código
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_TEXTFIELD_CODIGO}  30sec
    ${code}=                       generate random string  4  [NUMBERS]
    Set Test Variable              ${code}
    Input Text                     ${TRANSPORTADORA_TEXTFIELD_CODIGO}  FS${code}  

Então o Código inputado ficará evidente no textfield
    Textfield Should Contain       ${TRANSPORTADORA_TEXTFIELD_CODIGO}  FS${code}  

#Caso de Teste 02
Quando usuário inputar Nome
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_TEXTFIELD_NOME}  30sec
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Input Text                     ${TRANSPORTADORA_TEXTFIELD_NOME}  ${NOME}

Então o Nome inputado ficará evidente no textfield
    Textfield Should Contain       ${TRANSPORTADORA_TEXTFIELD_NOME}  ${NOME}

#Caso de Teste 03
Quando usuário inputar Nome Reduzido
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_TEXTFIELD_NOME_REDUZIDO}  30sec
    ${NOME_REDUZIDO}               FakerLibrary.First Name
    Set Test Variable              ${NOME_REDUZIDO}
    Input Text                     ${TRANSPORTADORA_TEXTFIELD_NOME_REDUZIDO}  ${NOME_REDUZIDO}

Então o Nome Reduzido inputado ficará evidente no textfield
    Textfield Should Contain       ${TRANSPORTADORA_TEXTFIELD_NOME_REDUZIDO}  ${NOME_REDUZIDO}

#Caso de Teste 04
Quando usuário preencher todos os campos obrigatórios e clicar no botão Incluir
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_TEXTFIELD_CODIGO}  30sec
    ${code}=                       generate random string  4  [NUMBERS]
    Set Test Variable              ${code}
    Input Text                     ${TRANSPORTADORA_TEXTFIELD_CODIGO}  FS${code}  
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_TEXTFIELD_NOME}  30sec
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Input Text                     ${TRANSPORTADORA_TEXTFIELD_NOME}  ${NOME}
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_TEXTFIELD_NOME_REDUZIDO}  30sec
    ${NOME_REDUZIDO}               FakerLibrary.First Name
    Set Test Variable              ${NOME_REDUZIDO}
    Input Text                     ${TRANSPORTADORA_TEXTFIELD_NOME_REDUZIDO}  ${NOME_REDUZIDO}
    Sleep  2
    Click Element                  ${TRANSPORTADORA_BTN_INCLUIR}

Então usuário irá incluir uma Transportadora com sucesso 
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 05
Quando usuário preencher os campos com dados inconsistente e clicar no botão Incluir
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_INCLUIR}  30sec
    Click Element                  ${TRANSPORTADORA_BTN_INCLUIR} 

Então usuário irá visualizar uma mensagem de alerta relatando erro na inclusão
    Wait Until Element Is Visible  xpath://span[contains(.,'ERRO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 06
Quando usuário selecionar uma Transportadora e clicar no botão Excluir
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'test')]  30sec
    Click Element                  xpath://td[contains(.,'test')] 
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_EXCLUIR}
    Click Element                  ${TRANSPORTADORA_BTN_EXCLUIR}
    Sleep  5
    
Então usuário irá visualizar modal Deletar
    Page Should Contain            Apagar Parametrização de Transportadora

#Caso de Teste 07
Quando usuário selecionar uma Transportadora, clicar no botão Excluir em seguida no botão Cancelar
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'test')]  30sec
    Click Element                  xpath://td[contains(.,'test')] 
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_EXCLUIR}
    Click Element                  ${TRANSPORTADORA_BTN_EXCLUIR}
    Sleep  5
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_CANCELAR}
    Click Element                  ${TRANSPORTADORA_BTN_CANCELAR}

Então usuário irá fechar modal Deletar
    Sleep  5
    Page Should Not Contain        Apagar Parametrização de Transportadora

#Caso de Teste 08
Quando usuário selecionar uma Transportadora, clicar no botão Excluir em seguida no botão OK
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'test')]  30sec
    Click Element                  xpath://td[contains(.,'test')] 
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_EXCLUIR}
    Click Element                  ${TRANSPORTADORA_BTN_EXCLUIR}
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_APAGAR}
    Click Element                  ${TRANSPORTADORA_BTN_APAGAR}
    
Então usuário irá Excluir uma Transportadora com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]
    

#Caso de Teste 09
Quando usuário selecionar uma Transportadora, alterar dados em seguida clicar no botão Editar
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Uber do Brasil Tecnologia Ltda')]  30sec
    Click Element                  xpath://td[contains(.,'Uber do Brasil Tecnologia Ltda')] 
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_EDITAR}
    Click Element                  ${TRANSPORTADORA_BTN_EDITAR}

Então usuário irá Editar uma Transportadora com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 10
Quando usuário selecionar uma Transportadora, alterar informações com dados inconsistente em seguida clicar no botão Editar
    Sleep  2
    Wait Until Element Is Visible  ${TRANSPORTADORA_BTN_EDITAR}
    Click Element                  ${TRANSPORTADORA_BTN_EDITAR}
    Sleep  5

Então usuário irá visualizar uma mensagem de alerta
    Wait Until Element Is Visible  xpath://span[contains(.,'ERRO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 11
Quando usuário clicar no radio button Inf Manual Entregador
    Sleep  2
    Wait Until Element Is Visible  css=.mat-row:nth-child(2) #toggleEntregador .mat-slide-toggle-thumb  30sec
    Click Element                  css=.mat-row:nth-child(2) #toggleEntregador .mat-slide-toggle-thumb
    Sleep  2    

Então usuário irá alterar o radio button Inf Manual Entregador
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 12
Quando usuário clicar no radio button Inf Manual Entrega
    Sleep  2
    Wait Until Element Is Visible  css=.mat-row:nth-child(2) #toggleEntrega .mat-slide-toggle-thumb  30sec
    Click Element                  css=.mat-row:nth-child(2) #toggleEntrega .mat-slide-toggle-thumb
    Sleep  2

Então usuário irá alterar o radio button Inf Manual Entrega
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

