*** Settings ***
Library     SeleniumLibrary
Library     String
Resource    ../pageobjects/LoginPage.robot

*** Variables ***
${HOME_URL}                        https://parceirofast-develop.fastshop.com.br/home
${HOME_TITLE}                      Parceiro Fast | Home
${HOME_BTN_FMS}                    xpath=//span[contains(.,'FMS')]   
${HOME_BTN_PAINEL_PEDIDOS_VENDAS}  xpath=//span[contains(.,'Painel de Pedidos de Vendas')]
${HOME_BTN_PAINEL_POS_VENDAS}      xpath=//span[contains(.,'Painel de Pós Vendas')]
${HOME_BTN_CONFIGURACOES_FMS}      xpath=//span[contains(.,'Configurações - FMS')]
${HOME_BTN_CADASTRO_USUARIOS_FMS}  xpath=//span[contains(.,'Cadastro de Usuários FMS')]
${HOME_BTN_SLA_TIPO_ENTREGA}       xpath=//span[contains(.,'SLA - Tipo Entrega')]
${HOME_BTN_SLA_STATUS}             xpath=//span[contains(.,'SLA - Status')]
${HOME_BTN_TRANSPORTADORA}         xpath=//span[contains(.,'Transportadora')]
${HOME_BTN_CADASTRO_LOJAS}         xpath=//span[contains(.,'Cadastro de Lojas')]
${IFRAME}                          id:iframePortal     

*** Keywords ***
Então usuário será direcionado para a page Home do Fast Shop Seller Center 
    Location Should Be             ${HOME_URL}

Dado que usuário acessa a página FMS/Painel de Pedidos de Vendas da Fast Shop Seller Center
    Go To                          ${LOGIN_URL}
    Wait Until Element Is Visible  ${LOGIN_EMAIL}
    Input Text                     ${LOGIN_EMAIL}  fastshop.admin.cadastroseller@mailinator.com
    Wait Until Element Is Visible  ${LOGIN_SENHA}
    Input Text                     ${LOGIN_SENHA}  Fast@123
    Wait Until Element Is Visible  ${LOGIN_BTN_ENTRAR}
    Click Element                  ${LOGIN_BTN_ENTRAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${HOME_BTN_FMS}  30sec
    Click Element                  ${HOME_BTN_FMS}
    Sleep  3
    Set Focus To Element           ${HOME_BTN_CONFIGURACOES_FMS}
    Sleep  2
    # Wait Until Element Is Visible  ${HOME_BTN_PAINEL_PEDIDOS_VENDAS}  30sec
    Click Element                  ${HOME_BTN_PAINEL_PEDIDOS_VENDAS}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Select Frame                   ${IFRAME} 

Dado que usuário acessa a página FMS/Painel de Pós Vendas da Fast Shop Seller Center
    Go To                          ${LOGIN_URL}
    Wait Until Element Is Visible  ${LOGIN_EMAIL}
    Input Text                     ${LOGIN_EMAIL}  fastshop.admin.cadastroseller@mailinator.com
    Wait Until Element Is Visible  ${LOGIN_SENHA}
    Input Text                     ${LOGIN_SENHA}  Fast@123
    Wait Until Element Is Visible  ${LOGIN_BTN_ENTRAR}
    Click Element                  ${LOGIN_BTN_ENTRAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${HOME_BTN_FMS}  30sec
    Click Element                  ${HOME_BTN_FMS}
    Sleep  3
    Wait Until Element Is Visible  ${HOME_BTN_PAINEL_POS_VENDAS}  30sec
    Click Element                  ${HOME_BTN_PAINEL_POS_VENDAS}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Select Frame                   ${IFRAME}   

Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    Go To                          ${LOGIN_URL}
    Wait Until Element Is Visible  ${LOGIN_EMAIL}
    Input Text                     ${LOGIN_EMAIL}  fastshop.admin.cadastroseller@mailinator.com
    Wait Until Element Is Visible  ${LOGIN_SENHA}
    Input Text                     ${LOGIN_SENHA}  Fast@123
    Wait Until Element Is Visible  ${LOGIN_BTN_ENTRAR}
    Click Element                  ${LOGIN_BTN_ENTRAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${HOME_BTN_CONFIGURACOES_FMS}  30sec
    Click Element                  ${HOME_BTN_CONFIGURACOES_FMS}
    Sleep  3
    Wait Until Element Is Visible  ${HOME_BTN_CADASTRO_USUARIOS_FMS}  30sec
    Click Element                  ${HOME_BTN_CADASTRO_USUARIOS_FMS}  
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Select Frame                   ${IFRAME}   

Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    Go To                          ${LOGIN_URL}
    Wait Until Element Is Visible  ${LOGIN_EMAIL}
    Input Text                     ${LOGIN_EMAIL}  fastshop.admin.cadastroseller@mailinator.com
    Wait Until Element Is Visible  ${LOGIN_SENHA}
    Input Text                     ${LOGIN_SENHA}  Fast@123
    Wait Until Element Is Visible  ${LOGIN_BTN_ENTRAR}
    Click Element                  ${LOGIN_BTN_ENTRAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${HOME_BTN_CONFIGURACOES_FMS}  30sec
    Click Element                  ${HOME_BTN_CONFIGURACOES_FMS}
    Sleep  3
    Wait Until Element Is Visible  ${HOME_BTN_SLA_TIPO_ENTREGA}  30sec
    Click Element                  ${HOME_BTN_SLA_TIPO_ENTREGA} 
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec  
    Select Frame                   ${IFRAME}     

Dado que usuário acessa a página Configurações - FMS/SLA - Status da Fast Shop Seller Center
    Go To                          ${LOGIN_URL}
    Wait Until Element Is Visible  ${LOGIN_EMAIL}
    Input Text                     ${LOGIN_EMAIL}  fastshop.admin.cadastroseller@mailinator.com
    Wait Until Element Is Visible  ${LOGIN_SENHA}
    Input Text                     ${LOGIN_SENHA}  Fast@123
    Wait Until Element Is Visible  ${LOGIN_BTN_ENTRAR}
    Click Element                  ${LOGIN_BTN_ENTRAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${HOME_BTN_CONFIGURACOES_FMS}  30sec
    Click Element                  ${HOME_BTN_CONFIGURACOES_FMS}
    Sleep  3    
    Wait Until Element Is Visible  ${HOME_BTN_SLA_STATUS}  30sec
    Click Element                  ${HOME_BTN_SLA_STATUS} 
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Select Frame                   ${IFRAME}     

Dado que usuário acessa a página Configurações - FMS/Transportadora da Fast Shop Seller Center
    Go To                          ${LOGIN_URL}
    Wait Until Element Is Visible  ${LOGIN_EMAIL}
    Input Text                     ${LOGIN_EMAIL}  fastshop.admin.cadastroseller@mailinator.com
    Wait Until Element Is Visible  ${LOGIN_SENHA}
    Input Text                     ${LOGIN_SENHA}  Fast@123
    Wait Until Element Is Visible  ${LOGIN_BTN_ENTRAR}
    Click Element                  ${LOGIN_BTN_ENTRAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${HOME_BTN_CONFIGURACOES_FMS}  30sec
    Click Element                  ${HOME_BTN_CONFIGURACOES_FMS}
    Sleep  3
    Wait Until Element Is Visible  ${HOME_BTN_TRANSPORTADORA}  30sec
    Click Element                  ${HOME_BTN_TRANSPORTADORA}    
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Select Frame                   ${IFRAME}

Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center 
    Go To                          ${LOGIN_URL}
    Wait Until Element Is Visible  ${LOGIN_EMAIL}
    Input Text                     ${LOGIN_EMAIL}  fastshop.admin.cadastroseller@mailinator.com
    Wait Until Element Is Visible  ${LOGIN_SENHA}
    Input Text                     ${LOGIN_SENHA}  Fast@123
    Wait Until Element Is Visible  ${LOGIN_BTN_ENTRAR}
    Click Element                  ${LOGIN_BTN_ENTRAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${HOME_BTN_CONFIGURACOES_FMS}  30sec
    Click Element                  ${HOME_BTN_CONFIGURACOES_FMS}
    Sleep  3
    Wait Until Element Is Visible  ${HOME_BTN_CADASTRO_LOJAS}  30sec
    Click Element                  ${HOME_BTN_CADASTRO_LOJAS} 
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Select Frame                   ${IFRAME}

