*** Settings ***
Library     DateTime
Library     OperatingSystem
Library     SeleniumLibrary
Library     String
Library     FakerLibrary    locale=pt_BR
Resource    ../pageobjects/HomePage.robot

*** Variables ***
${CADASTRO_DE_LOJAS_URL}                        https://parceirofast-develop.fastshop.com.br/home/painel
${CADASTRO_DE_LOJAS_TITLE}                      Parceiro Fast | Cadastro de Lojas
${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}        id:btnNovaStore
${CADASTRO_DE_LOJAS_COMBO_SELLER}               id:comboSeller
${CADASTRO_DE_LOJAS_COMBO_WEBSTORE}             id:comboWebStore
${CADASTRO_DE_LOJAS_TEXTFIELD_CNPJ}             id:cnpj
${CADASTRO_DE_LOJAS_TEXTFIELD_DESCRICAO}        id:nomeStore
${CADASTRO_DE_LOJAS_TEXTFIELD_FILIAL}           id:filial
${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}              id:cep
${CADASTRO_DE_LOJAS_ICONE_CEP}                  id:iconeBuscarCep
${CADASTRO_DE_LOJAS_TEXTFIELD_ENDERECO}         id:endereco
${CADASTRO_DE_LOJAS_TEXTFIELD_NUMERO}           id:numero
${CADASTRO_DE_LOJAS_TEXTFIELD_BAIRRO}           id:bairro
${CADASTRO_DE_LOJAS_TEXTFIELD_CIDADE}           id:cidade
${CADASTRO_DE_LOJAS_TEXTFIELD_UF}               id:uf
${CADASTRO_DE_LOJAS_TEXTFIELD_COMPLEMENTO}      id:complemento
${CADASTRO_DE_LOJAS_TEXTFIELD_LATITUDE}         id:latitude
${CADASTRO_DE_LOJAS_TEXTFIELD_LONGITUDE}        id:longitude
${CADASTRO_DE_LOJAS_TEXTFIELD_ID_DA_LOJA}       id:idStore
${CADASTRO_DE_LOJAS_TEXTFIELD_INSTRUCOES}       id:instrucoesRetirada
${CADASTRO_DE_LOJAS_BTN_SALVAR}                 id:salvarStore
${CADASTRO_DE_LOJAS_BTN_EDITAR}                 id:btnEditar
${CADASTRO_DE_LOJAS_BTN_DELETAR}                id:btnDeletar
${CADASTRO_DE_LOJAS_TABLE}                      id:gridLojas
${CADASTRO_DE_LOJAS_BTN_CANCELAR}               id:buttonSubmitCancel
${CADASTRO_DE_LOJAS_BTN_APAGAR}                 id:buttonSubmitApagar

*** Keywords ***
#Caso de Teste 01
Quando usuário clicar no botão Criar Nova Loja
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}

Então usuário será direcionado para a página de Cadastro de Lojas
    Sleep  5
    Page Should Contain            Cadastro de Lojas

#Caso de Teste 02
Quando clicar no botão Criar Nova Loja e selecionar uma opção no comboBox Seller
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Sleep  3
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_COMBO_SELLER}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'ELECTROLUXQA')] 

Então a opção selecionada no comboBox Seller ficará evidente 
    ${selected}=                   Get Text  ${CADASTRO_DE_LOJAS_COMBO_SELLER}
    Should Be Equal                ${selected}  ELECTROLUXQA 

#Caso de Teste 03
Quando clicar no botão Criar Nova Loja e selecionar uma opção no comboBox WebStore
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_COMBO_SELLER}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'ELECTROLUXQA')] 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_COMBO_WEBSTORE}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_COMBO_WEBSTORE}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'MKT_ELECTROLUX')] 

Então a opção selecionada no comboBox WebStore ficará evidente 
    ${selected}=                   Get Text  ${CADASTRO_DE_LOJAS_COMBO_WEBSTORE}
    Should Be Equal                ${selected}  MKT_ELECTROLUX 

#Caso de Teste 04
Quando usuário clicar no botão Criar Nova Loja e inputar CNPJ
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${CNPJ}                        FakerLibrary.CNPJ
    Set Test Variable              ${CNPJ}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_CNPJ}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_CNPJ}  ${CNPJ}

Então o CNPJ inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_CNPJ}  ${CNPJ}

#Caso de Teste 05
Quando usuário clicar no botão Criar Nova Loja e inputar Descrição
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_DESCRICAO}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_DESCRICAO}  test

Então o Descrição inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_DESCRICAO}  test

#Caso de Teste 06
Quando usuário clicar no botão Criar Nova Loja e inputar Filial
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_FILIAL}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_FILIAL}  test

Então o Filial inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_FILIAL}  test

#Caso de Teste 07
Quando usuário clicar no botão Criar Nova Loja e inputar CEP
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${CEP}                         FakerLibrary.Postcode
    Set Test Variable              ${CEP}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  04007900 

Então o CEP inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  04.007-900 

#Caso de Teste 08
Quando usuário clicar no botão Criar Nova Loja, inputar um CEP válido e clicar no ícone de pesquisa
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${CEP}                         FakerLibrary.Postcode
    Set Test Variable              ${CEP}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  04007900 
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_ICONE_CEP}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_ICONE_CEP}  
    Sleep  2

Então os campos de endereço são preenchidos com os dados do CEP digitado
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  04.007-900 
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_ENDERECO}  Rua Tutóia 1157
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_BAIRRO}  Vila Mariana
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_CIDADE}  São Paulo
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_UF}  SP

#Caso de Teste 09
Quando usuário clicar no botão Criar Nova Loja, inputar um CEP inválido e clicar no ícone de pesquisa
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${CEP}                         FakerLibrary.Postcode
    Set Test Variable              ${CEP}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  99999999 
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_ICONE_CEP}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_ICONE_CEP}  
    Sleep  2

Então usuário irá visualizar uma mensagem de alerta
    Sleep  5
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 10
Quando usuário clicar no botão Criar Nova Loja e inputar Endereço
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${ENDERECO}                    FakerLibrary.Address
    Set Test Variable              ${ENDERECO}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_ENDERECO}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_ENDERECO}  Rua Tutóia

Então o Endereço inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_ENDERECO}  Rua Tutóia

#Caso de Teste 11
Quando usuário clicar no botão Criar Nova Loja e inputar Número
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_NUMERO}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_NUMERO}  1157

Então o Número inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_NUMERO}  1157

#Caso de Teste 12
Quando usuário clicar no botão Criar Nova Loja e inputar Bairro
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${BAIRRO}                      FakerLibrary.Neighborhood
    Set Test Variable              ${BAIRRO}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_BAIRRO}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_BAIRRO}  ${BAIRRO} 

Então o Bairro inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_BAIRRO}  ${BAIRRO} 

#Caso de Teste 13
Quando usuário clicar no botão Criar Nova Loja e inputar Cidade
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${CIDADE}                      FakerLibrary.City
    Set Test Variable              ${CIDADE}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_CIDADE}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_CIDADE}  ${CIDADE} 

Então a Cidade inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_CIDADE}  ${CIDADE} 

#Caso de Teste 14
Quando usuário clicar no botão Criar Nova Loja e inputar UF
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${UF}                          FakerLibrary.State
    Set Test Variable              ${UF}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_UF}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_UF}  ${UF}

Então o UF inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_UF}  ${UF}

#Caso de Teste 15
Quando usuário clicar no botão Criar Nova Loja e inputar Complemento
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_COMPLEMENTO}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_COMPLEMENTO}  prédio

Então o Complemento inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_COMPLEMENTO}  prédio

#Caso de Teste 16
Quando usuário clicar no botão Criar Nova Loja e inputar Latitude
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_LATITUDE}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_LATITUDE}  test

Então o Latitude inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_LATITUDE}  test

#Caso de Teste 17
Quando usuário clicar no botão Criar Nova Loja e inputar Longitude
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_LONGITUDE}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_LONGITUDE}  test

Então o Longitude inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_LONGITUDE}  test

#Caso de Teste 18
Quando usuário clicar no botão Criar Nova Loja e inputar ID da Loja
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    ${ID}=                         generate random string  3  [NUMBERS]
    Set Test Variable              ${ID}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_ID_DA_LOJA}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_ID_DA_LOJA}  ${ID}

Então o ID da Loja inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_ID_DA_LOJA}  ${ID}

#Caso de Teste 19
Quando usuário clicar no botão Criar Nova Loja e inputar Instruções de Retirada
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_INSTRUCOES}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_INSTRUCOES}  test

Então as Instruções de Retirada inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_LOJAS_TEXTFIELD_INSTRUCOES}  test

#Caso de Teste 20
Quando usuário clicar no botão Criar Nova Loja, preencher todos os campos corretamente e clicar no botão Salvar 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CRIAR_NOVA_LOJA}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_COMBO_SELLER}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'ELECTROLUXQA')] 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_COMBO_WEBSTORE}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_COMBO_WEBSTORE}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'MKT_ELECTROLUX')] 
    Sleep  2
    ${CNPJ}                        FakerLibrary.CNPJ
    Set Test Variable              ${CNPJ}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_CNPJ}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_CNPJ}  ${CNPJ}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_DESCRICAO}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_DESCRICAO}  test
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_FILIAL}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_FILIAL}  test
    Sleep  2
    ${CEP}                         FakerLibrary.Postcode
    Set Test Variable              ${CEP}
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_CEP}  04007900 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_ICONE_CEP}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_ICONE_CEP}  
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_NUMERO}  30sec
    ${NUMERO}=                     generate random string  3  [NUMBERS]
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_NUMERO}  ${NUMERO}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_COMPLEMENTO}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_COMPLEMENTO}  prédio
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_LATITUDE}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_LATITUDE}  test
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_LONGITUDE}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_LONGITUDE}  test
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_ID_DA_LOJA}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_ID_DA_LOJA}  529
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_TEXTFIELD_INSTRUCOES}  30sec
    Input Text                     ${CADASTRO_DE_LOJAS_TEXTFIELD_INSTRUCOES}  test
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_SALVAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_SALVAR}
    Sleep  2

Então usuário irá conseguir efetuar um Cadastro de Lojas com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 21
Quando usuário clicar no botão Editar, alterar informações do cadastro da loja e clicar no botão Salvar 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_EDITAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_EDITAR}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_SALVAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_SALVAR}

Então usuário irá conseguir Editar um Cadastro de Lojas com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 22
Quando usuário clicar no botão Deletar
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_DELETAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_DELETAR}
    Sleep  5
    
Então usuário irá visualizar modal Deletar
    Page Should Contain            Apagar uma loja  

#Caso de Teste 23
Quando usuário clicar no botão Deletar em seguida no botão Cancelar 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_DELETAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_DELETAR}
    Sleep  5
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_CANCELAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_CANCELAR}

Então usuário irá fechar modal Deletar
    Sleep  2
    Page Should Not Contain        Apagar uma loja  

#Caso de Teste 24
Quando usuário clicar no botão Deletar em seguida no botão OK 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_DELETAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_DELETAR}
    Sleep  5
    Wait Until Element Is Visible  ${CADASTRO_DE_LOJAS_BTN_APAGAR}  30sec
    Click Element                  ${CADASTRO_DE_LOJAS_BTN_APAGAR}

Então usuário irá Excluir um Cadastro de Lojas com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]
