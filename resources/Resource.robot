*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${HEADLESS}     headlesschrome 
${OPTIONS}      add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox"); add_experimental_option('excludeSwitches', ['enable-logging'])
${BROWSER}      firefox
${CHROME}       chrome  
${IE}           ie
${URL}          https://parceirofast-develop.fastshop.com.br/

*** Keywords ***
#### Setup e Teardown
Abrir navegador
    Open Browser    about:blank  ${BROWSER}  
    maximize browser window

Abrir navegador chrome
    Open Browser    about:blank  ${CHROME}
    maximize browser window

Abrir navegador internet
    Open Browser    about:blank  ${IE}
    maximize browser window

Abrir navegador headless
    Open Browser    about:blank  ${HEADLESS}  options=${OPTIONS}
    maximize browser window

Fechar navegador
    Run Keyword And Ignore Error   Capture Page Screenshot
    Close Browser
    

